QGIS PLUGIN: MTOP OPEN DATA
----------------------------

Este plugin reúne los servicios webs geográficos (WMS y WFS) de
Uruguay provistos por el Ministerio de Transporte y Obras Públicas (MTOP) siendo
una adaptación del plugin existente [GreekOpenData](https://github.com/thaura/GreekOpenData)
creado por [Eirini Simitzi](mailto:simitzi.irini@gmail.com?Subject=GreekOpenData) 
y [Thanos Strantzalis](mailto:thanos.strantzalis@gmail.com?Subject=GreekOpenData).

El objetivo del plugin es facilitar el acceso a la información proporcionada por el MTOP
permitiendo visualizarla y descargarla fácilmente. Las capas de datos son
un sub conjunto de las disponibles actualmente.
La totalidad de las capas puede ser consultada a través de la web del
equipo: [https://geoportal.mtop.gub.uy/](https://geoportal.mtop.gub.uy/)

Para poder utilizar este plugin es necesario contar con conexión a internet.

La adaptación ha sido realizada por el [Equipo IDE MTOP](mailto:geoportal@mtop.gub.uy?Subject=MTOPOpenData).

Este plugin es liberado bajo la licencia GNU versión 3.

# Instalación
## Administrador de Complementos
La última versión estable del plugin se puede descargar e instalar desde el
administrador de complementos de QGIS buscándolo por su nombre y presionando el 
botón instalar complemento (o actualizar complemento en caso de que ya se encuentre
una versión instalada).

## Manual
La instalación puede realizarse en forma manual descargando este repositorio
ejecutando:

```sh
$ git clone https://gitlab.com/IDE_MTOP_PUBLICO/MTOPOpenData.git
```

>ACLARACIÓN: El branch master contiene la versión en desarrollo, y los tags vX.Y
indican las versiones estables liberadas.

Para poder instalar el plugin es necesario tener instalada la herramienta [pb_tool](http://g-sherman.github.io/plugin_build_tool/).
Es recomendable trabajar en un entorno virtual para evitar posibles 
inconvenientes con el sistema, por esto es necesario tener instalado virtualenv.
En Debian Linux podemos instalarlo ejecutando:

```sh
$ sudo apt-get install virtualenvwrapper
```

Para inicializar el entorno virtual MTOPOpenData, podemos correr los comandos:

```sh
$ mkvirtualenv MTOPOpenData
$ workon MTOPOpenData
```

Posterior a esto podemos instalar pb_tool utilizando pip ejecutando:

```sh
MTOPOpenData$ pip install pb_tool
```

Para terminar la instalación es necesario ejecutar:

```sh
MTOPOpenData$ pb_tool deploy
```

El comando genera el archivo resources.py y mueve los archivos necesarios a la 
carpeta de plugins de QGIS (por defecto es /home/<username>/.qgis/python/plugins).

>Si se elige instalarlo de esta manera se debe tener en cuenta que ante futuras 
actualizaciones se deberá repetir este proceso.

# Configuración
## MTOPOpenData.cfg
Este archivo maneja dos elementos:
- info: dirección a donde la interfaz redirige al presionar el botón Info. Se 
utiliza para mostrar la web del proyecto desde la cual el usuario podrá obtener 
más información sobre los datos.
- sources: dirección a el archivo remoto data.min.json Se utiliza para verificar
si se han realizado actualizaciones en los conjuntos de datos publicados.

## Datos y Publicación
La aplicación lee el archivo data.min.json para mostrar los elementos de la 
tabla de capas. Se cuenta con el archivo data.json en el cual cada entrada se 
corresponde con un elemento de la tabla.
Cada elemento se encuentra representado como:

```javascript
 {
    "Num": "", /* en desuso */
    "Name_ES": "Cadena Granos (Arroz)", /* nombre en español de la capa de datos */
                                        /* según se muestra en la tabla */
    "Source_ES": "Ministerio de Transporte y Obras Públicas", /* organización responsable de la */
                                                              /* capa de datos según se muestra en la tabla */
    "Name_EN": "",              /* en desuso */
    "Source_EN": "",            /* en desuso */
    "Last revision": "",        /* fecha de última revisión del dato */
    "Created date": "",         /* en desuso */
    "Quick look name": "icon",  /* en desuso */
    "Description_EN": "",       /* en desuso */
    "Description_ES": "",       /* descripción en español, si no es vacío         */
                                /* sobreescribe la info del servidor de metadatos */
    "Type": ["WMS", "WFS"],     /* lista de tipos de servicio publicado (sólo WMS y WFS soportados) */
    "Layer_Name": "inf_tte_ttelog_logistica:cadena_granos_arroz", /* nombre de la capa de datos a */
                                                                  /* publicar según el servidor de mapas */
    "server": "https://geoservicios.mtop.gub.uy/geoserver",  /* dirección del servidor de mapas donde está la */
                                                            /* capa de datos */
    "metadata_server": "https://geoservicios.mtop.gub.uy",   /* dirección del servidor de metadatos (sólo geonetwork) */
                                                            /* se asume que la dirección final del servidor será de  */
                                                            /* la forma https://geoservicios.mtop.gub.uy/geonetwork/  */
    "uuid" : "db79ef13-e7b5-40cf-a32d-7deb027ca3f9" /* identificador uuid del metadato en el servidor */
},
```

Para publicar los datos es recomendable minimizar data.json sacando los carácteres
innecesarios (por ejemplo espacios y tabs), de esta forma reducimos la cantidad 
de datos descargados al iniciar el plugin.
Se encuentra disponible el script minimize_data.py el cual al ser ejecutado en 
la ruta del proyecto genera la versión minimizada data.min.json, este archivo 
debe ser copiado a la dirección especificada en MTOPOpenData.cfg.