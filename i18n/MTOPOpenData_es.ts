<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>MTOPOpenDataDialogBase</name>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="32"/>
        <source>MTOP Open Data</source>
        <translatorcomment>MTOP Open Data</translatorcomment>
        <translation>MTOP Open Data</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="56"/>
        <source>Info</source>
        <translatorcomment>Info</translatorcomment>
        <translation>Info</translation>
    </message>
    <message utf8="true">
        <location filename="../MTOPOpenData_dialog_base.ui" line="63"/>
        <source>Descripción</source>
        <translatorcomment>Descripción</translatorcomment>
        <translation>Descripción</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="81"/>
        <source>Cargar</source>
        <translatorcomment>Cargar</translatorcomment>
        <translation>Cargar</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="97"/>
        <source>Cerrar</source>
        <translatorcomment>Cerrar</translatorcomment>
        <translation>Cerrar</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="191"/>
        <source>Nombre</source>
        <translatorcomment>Nombre</translatorcomment>
        <translation>Nombre</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="202"/>
        <source>Tipo</source>
        <translatorcomment>Tipo</translatorcomment>
        <translation>Tipo</translation>
    </message>
    <message utf8="true">
        <location filename="../MTOPOpenData_dialog_base.ui" line="213"/>
        <source>Organización</source>
        <translatorcomment>Organización</translatorcomment>
        <translation>Organización</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="236"/>
        <source>Buscar</source>
        <translatorcomment>Buscar</translatorcomment>
        <translation>Buscar</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="302"/>
        <source>Vista Previa</source>
        <translatorcomment>Vista Previa</translatorcomment>
        <translation>Vista Previa</translation>
    </message>
</context>
</TS>
