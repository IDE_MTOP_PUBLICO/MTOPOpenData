<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en_US">
<context>
    <name>MTOPOpenDataDialogBase</name>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="32"/>
        <source>MTOP Open Data</source>
        <translatorcomment>MTOP Open Data</translatorcomment>
        <translation>MTOP Open Data</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="56"/>
        <source>Info</source>
        <translatorcomment>Info</translatorcomment>
        <translation>Info</translation>
    </message>
    <message utf8="true">
        <location filename="../MTOPOpenData_dialog_base.ui" line="63"/>
        <source>Descripción</source>
        <translatorcomment>Description</translatorcomment>
        <translation>Description</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="81"/>
        <source>Cargar</source>
        <translatorcomment>Load</translatorcomment>
        <translation>Load</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="97"/>
        <source>Cerrar</source>
        <translatorcomment>Close</translatorcomment>
        <translation>Close</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="191"/>
        <source>Nombre</source>
        <translatorcomment>Name</translatorcomment>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="202"/>
        <source>Tipo</source>
        <translatorcomment>Type</translatorcomment>
        <translation>Type</translation>
    </message>
    <message utf8="true">
        <location filename="../MTOPOpenData_dialog_base.ui" line="213"/>
        <source>Organización</source>
        <translatorcomment>Organization</translatorcomment>
        <translation>Organization</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="236"/>
        <source>Buscar</source>
        <translatorcomment>Search</translatorcomment>
        <translation>Search</translation>
    </message>
    <message>
        <location filename="../MTOPOpenData_dialog_base.ui" line="302"/>
        <source>Vista Previa</source>
        <translatorcomment>Preview</translatorcomment>
        <translation>Preview</translation>
    </message>
</context>
</TS>
