#!/usr/bin/python
import json
import os

f_orig = os.path.join(os.path.dirname(__file__),"data.json")
f_min = os.path.join(os.path.dirname(__file__),"data.min.json")


# open extended data file
fd_orig = open(f_orig, "rb")
reader = json.load(fd_orig)
tmp = json.dumps(reader, separators=(',',':'))
fd_orig.close()

# create minimized data file for upload to qgis plugin repo
fd_min = open(f_min, "w")
fd_min.write(tmp)
fd_min.close()


