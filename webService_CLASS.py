# -*- coding: utf-8 -*-

from builtins import object
import urllib.request, urllib.error, urllib.parse
from xml.etree import ElementTree as ET
import os
from datetime import datetime

class WebServiceParams(object):

    def __init__(self, name, nameES,nameEn,sourceES,sourceEN,creationDate,
                 descEN,descES,serviceType, layerName,server,metadataServer, uuid):
        self.name = name
        self.nameES = nameES
        self.nameEn = nameEn
        self.creationDate = creationDate
        # self.lastUpdate = lastUpdate
        self.sourceES = sourceES
        self.sourceEN = sourceEN
        self.descES = descES
        self.descEN = descEN
        self.serviceType = serviceType
        self.layerName = layerName
        self.server = server
        self.metadataServer = metadataServer
        self.QLname = "icon.png"
        self.uuid = uuid # geonetwork uuid
        self.desc = ""

    def getName(self, language):
        # check the language
        if language == "EN":
            name = self.nameEn
        elif language == "ES":
            name = self.nameES
        # encode to utf-8
        #name = unicode(name, 'utf-8')
        return name

    def getSource(self, language):
        # check the language
        if language == "EN":
            src = self.sourceEN
        elif language == "ES":
            src = self.sourceES
        # encode to utf-8
        #src = unicode(src, 'utf-8')
        return src


    def loadOnlineData(self, language, quicklooks_dir):
        # check the language
        if not self.desc:
            if language == "EN":
                self.desc = self.descEN
            elif language == "ES":
                self.desc = self.descES

            # if uuid defined, search info in metadata server
            # only for geonetwork!!
            if not self.desc and self.uuid and self.metadataServer:
                file = urllib.request.urlopen(self.metadataServer+'/geonetwork/srv/es/rss.search?uuid='+self.uuid)
                data = file.read()
                root = ET.fromstring(data)
                namespaces = {'media': 'http://search.yahoo.com/mrss/'}
                items = root.findall('./channel/item', namespaces)
                self.desc = items[0].find('description').text
                self.creationDate = items[0].find('pubDate').text
                file_path = os.path.join(quicklooks_dir, self.name)
                self.QLname = self.name
                try:
                    imageUrl = items[0].find('./media:content', namespaces).attrib['url']
                    file = urllib.request.urlopen(imageUrl)
                    data = file.read()
                    # fixme: prevent download if file exist (too slow vs always download)
                    # if os.path.exists(file_path):
                    #     metadataUpdateDate = datetime.strptime(self.creationDate[:20], '%d %b %Y %H:%M:%S')
                    #     imgUpdateDate = datetime.fromtimestamp(os.path.getmtime(file_path))
                    #     if metadataUpdateDate <= imgUpdateDate:
                    #         return
                    # descargar
                    with open(file_path, "w+b") as img:
                        img.write(data)
                        img.close()
                    file.close()
                except:
                    if not os.path.exists(file_path):
                        self.QLname = "icon.png"

    def webServiceParams(self):

        if self.serviceType == "WMS":
            ignoreRequests = "IgnoreGetFeatureInfoUrl=1&IgnoreGetMapUrl=1&"

            layers = "&layers="+self.layerName[1]
            styles = "&styles="
            url = "&url=" + self.server + "/" + self.layerName[0] + "/" + self.layerName[1] +"/wms?"
            EPSG = "EPSG:4326"
            params = "version=1.1.1&contextualWMSLegend=0&crs=" + EPSG +"&dpiMode=7&featureCount=10&format=image/png"

            #construct url to call the webService
            urlWithParams = ignoreRequests + params + layers +  styles +  url
            return  urlWithParams

        elif self.serviceType == "WFS":
            url = self.server + "/ows?"
            getFeatureRequest = "SERVICE=WFS&VERSION=1.0.0&REQUEST=GetFeature"
            layer = "&TYPENAME=" + self.layerName[0] + ":" + self.layerName[1]
            EPSG = "&SRSNAME=EPSG:4326"

            urlWithParams = url + getFeatureRequest + layer + EPSG
            return  urlWithParams