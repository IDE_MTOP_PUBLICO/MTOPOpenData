//script para ejecutar en geoportal.mtop.gub.uy/geoservicios y obtener un listado de las capas disponibles por el geoportal en el formato de salida deseado
listadoCapasParaQgis = [];
jQuery('[data-fieldname=capa]').each((index, capa) => {
    let layer, nombreCapa, uuid;
    jQuery('div.form-group input', capa).each((i, d) => {
        switch (i) {
            case 0:
                nombreCapa = jQuery(d).val();
                break;
            case 1:
                layer = jQuery(d).val();
                break;
            case 2:
                layer += ':' + jQuery(d).val();
                break;
            case 6:
                uuid = jQuery(d).val();
                break;
        }
    });
    console.log(layer, nombreCapa, uuid);
    listadoCapasParaQgis.push({
        "uuid": uuid,
        "Source_EN": "",
        "Name_ES": nombreCapa,
        "metadata_server": "https://geoservicios.mtop.gub.uy",
        "Description_EN": "",
        "server": "https://geoservicios.mtop.gub.uy/geoserver",
        "Created date": "",
        "Description_ES": "",
        "Num": "",
        "Layer_Name": layer,
        "Name_EN": "",
        "Source_ES": "Ministerio de Transporte y Obras Públicas",
        "Type": ["WMS", "WFS"],
        "Last revision": ""
    });
});
listadoCapasParaQgis = listadoCapasParaQgis.filter(c => !c.Layer_Name.includes('mb_piria') && !c.Layer_Name.includes('mb_hervidero') && !c.Layer_Name.includes('mb_sayago') && !c.Layer_Name.includes('mb_pap'));
listadoCapasParaQgis = listadoCapasParaQgis.sort((a, b) => a.Name_ES.localeCompare(b.Name_ES));
console.log(JSON.stringify(listadoCapasParaQgis));